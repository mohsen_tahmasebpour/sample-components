import React from 'react';
import './style.scss';
import Accordion from '../../components/accordion';
import { accordionData } from './seeder';


function App() {
	return (
		<div className="page-wrapper">
			<div className="header">
				<div className="date">
					Reactjs BootCamp 1399-05
				</div>
				<div className="title">
					Make your component then add here as a sample.
				</div>
			</div>
			<div className="main-container">
				<div className="card">
					<Accordion
						items={accordionData}
					/>
				</div>
				<div className="card">YOUR COMPONENT</div>
				<div className="card">YOUR COMPONENT</div>
				<div className="card">YOUR COMPONENT</div>
				<div className="card">YOUR COMPONENT</div>
				<div className="card">YOUR COMPONENT</div>
			</div>
		</div>
	);
}

export default App;
